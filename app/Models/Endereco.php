<?php

namespace App\Models;

use App\ValueObjects\Cep;

class Endereco
{
    private $cep;
    private $logradouro;
    private $bairro;
    private $numero;

    public function __construct(Cep $cep)
    {
        $this->cep = $cep;
    }

    /**
     * @return String
     */
    public function getLogradouro() : String
    {
        return $this->logradouro;
    }

    /**
     * @param String $logradouro
     */
    public function setLogradouro(String $logradouro) : void
    {
        $this->logradouro = $logradouro;
    }

    /**
     * @return String
     */
    public function getBairro() : String
    {
        return $this->bairro;
    }

    /**
     * @param String $bairro
     */
    public function setBairro(String $bairro) : void
    {
        $this->bairro = $bairro;
    }

    /**
     * @return String
     */
    public function getNumero() : String
    {
        return $this->numero;
    }

    /**
     * @param String $numero
     */
    public function setNumero(String $numero) : void
    {
        $this->numero = $numero;
    }
}