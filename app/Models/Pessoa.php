<?php

namespace App\Models;

use App\ValueObjects\Cpf;
use App\ValueObjects\Email;
use App\ValueObjects\Nome;

class Pessoa
{
    private $nome;
    private $cpf;
    private $endereco;
    private $email;

    /**
     * Pessoa constructor.
     * @param Nome $nome
     * @param Cpf $cpf
     */
    public function __construct(Nome $nome, Cpf $cpf)
    {
        $this->nome = $nome;
        $this->cpf = $cpf;
    }

    /**
     * @param Endereco $endereco
     */
    public function setEndereco(Endereco $endereco) : void
    {
        $this->endereco = $endereco;
    }

    /**
     * @param Email $email
     */
    public function setEmail(Email $email) : void
    {
        $this->email = $email;
    }

    /**
     * @return Nome
     */
    public function getNome() : Nome
    {
        return $this->nome;
    }

    /**
     * @return Cpf
     */
    public function getCpf() : Cpf
    {
        return $this->cpf;
    }

    /**
     * @return Endereco
     */
    public function getEndereco() : Endereco
    {
        return $this->endereco;
    }

    /**
     * @return Email
     */
    public function getEmail() : Email
    {
        return $this->email;
    }
}