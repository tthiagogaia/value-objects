<?php

namespace App\Models;

use App\ValueObjects\Cnpj;

class Empresa
{
    private $cnpj;
    private $endereco;

    /**
     * Empresa constructor.
     * @param Cnpj $cnpj
     * @param Endereco $endereco
     */
    public function __construct(Cnpj $cnpj, Endereco $endereco)
    {
        $this->cnpj = $cnpj;
        $this->endereco = $endereco;
    }

    /**
     * @return Cnpj
     */
    public function getCnpj() : Cnpj
    {
        return $this->cnpj;
    }

    /**
     * @return Endereco
     */
    public function getEndereco() : Endereco
    {
        return $this->endereco;
    }
}