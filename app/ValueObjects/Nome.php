<?php

namespace App\ValueObjects;

use App\Expcetions\InvalidNomeException;

class Nome
{
    private $nome;

    /**
     * Nome constructor.
     * @param String $nome
     * @throws InvalidNomeException
     */
    public function __construct(String $nome)
    {
        $this->nome = $nome;
        $this->validate();
    }

    /**
     * @throws InvalidNomeException
     */
    private function validate()
    {
        $this->stringVerify();

        $this->emptyVerify();

        $this->lengthVerify();
    }

    /**
     * @throws InvalidNomeException
     */
    private function stringVerify()
    {
        if (! is_string($this->nome)) throw new InvalidNomeException('O Nome tem que ser String');
    }

    /**
     * @throws InvalidNomeException
     */
    private function emptyVerify() : void
    {
        if (empty($this->nome)) throw new InvalidNomeException('Nome em branco');
    }

    /**
     * @throws InvalidNomeException
     */
    private function lengthVerify() : void
    {
        if (strlen($this->nome) <= 2) throw new InvalidNomeException('Nome deve possuir mais de 2 caracteres');
    }

    /**
     * @return String
     */
    public function getNome() : String
    {
        return $this->nome;
    }
}