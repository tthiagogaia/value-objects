<?php

namespace App\ValueObjects;

use App\Expcetions\InvalidCpfException;
use App\ValueObjects\Traits\Validable;

class Cpf
{
    use Validable;

    private $cpf;

    /**
     * Cpf constructor.
     * @param String $cpf
     * @throws InvalidCpfException
     */
    public function __construct(String $cpf)
    {
        $this->cpf = $cpf;
        $this->validate();
    }

    /**
     * @throws InvalidCpfException
     */
    private function validate() : void
    {
        $this->emptyVerify();

        $this->maskRemove();

        $this->lengthVerify();

        $this->invalidDigitsVerify();

        $this->algorithmCpfVerify();
    }

    /**
     * @throws InvalidCpfException
     */
    private function emptyVerify() : void
    {
         if (empty($this->cpf)) throw new InvalidCpfException('CPF em branco');
    }

    private function maskRemove() : void
    {
        $this->cpf = preg_replace("/[^0-9]/", "", $this->cpf);
    }

    /**
     * @throws InvalidCpfException
     */
    private function lengthVerify() : void
    {
        if (strlen($this->cpf) != 11) throw new InvalidCpfException('CPF com quantidade de caracteres inválido');
    }

    /**
     * @throws InvalidCpfException
     */
    private function invalidDigitsVerify() : void
    {
        $invalids = [
            '00000000000', '11111111111', '22222222222', '33333333333', '44444444444',
            '55555555555', '66666666666', '77777777777', '88888888888', '99999999999'
        ];

        if (\in_array($this->cpf, $invalids)) throw new InvalidCpfException('CPF com todos os dígitos iguais');
    }

    /**
     * @throws InvalidCpfException
     */
    private function algorithmCpfVerify() : void
    {
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $this->cpf{$c} * (($t + 1) - $c);
            }

            $d = ((10 * $d) % 11) % 10;

            if ($this->cpf{$c} != $d) throw new InvalidCpfException('CPF inválido');
        }
    }

    /**
     * @return String
     */
    public function getCpf() : String
    {
        return $this->cpf;
    }
}
