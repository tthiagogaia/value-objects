<?php

namespace App\ValueObjects\Traits;

use App\Expcetions\MustBeImplementedException;

trait Validable
{
    /**
     * @throws MustBeImplementedException
     */
    private function emptyVerify() : void
    {
        throw new MustBeImplementedException('O método emptyVerify deve ser implementado');
    }

    /**
     * @throws MustBeImplementedException
     */
    private function lengthVerify() : void
    {
        throw new MustBeImplementedException('O método lengthVerify deve ser implementado');
    }
}