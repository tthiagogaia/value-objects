<?php

namespace App\ValueObjects;

use App\Expcetions\InvalidCepException;
use App\ValueObjects\Traits\Validable;

class Cep
{
    use Validable;

    private $cep;

    /**
     * Cnpj constructor.
     * @param String $cep
     * @throws InvalidCepException
     */
    public function __construct(String $cep)
    {
        $this->cep = $cep;
        $this->validate();
    }

    /**
     * @throws InvalidCepException
     */
    private function validate() : void
    {
        $this->emptyVerify();

        $this->maskRemove();

        $this->lengthVerify();

        $this->cepVerify();
    }

    /**
     * @throws InvalidCepException
     */
    private function emptyVerify() : void
    {
        if (empty($this->cep)) throw new InvalidCepException('CEP em branco');
    }

    private function maskRemove() : void
    {
        $this->cep = preg_replace('/[^0-9]/', '', $this->cep);
    }

    /**
     * @throws InvalidCepException
     */
    private function lengthVerify()
    {
        if (strlen($this->cep) < 8) throw new InvalidCepException('CEP com quantidade de caracteres inválido');
    }

    /**
     * @throws InvalidCepException
     */
    private function cepVerify()
    {
        if (! preg_match('/^[0-9]{5,5}([- ]?[0-9]{3,3})?$/', $this->cep))
            throw new InvalidCepException('CEP inválido');
    }

    /**
     * @return String
     */
    public function getCep() : String
    {
        return $this->cep;
    }
}
