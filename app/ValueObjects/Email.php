<?php

namespace App\ValueObjects;

use App\Expcetions\InvalidEmailException;
use App\ValueObjects\Traits\Validable;

class Email
{
    use Validable;

    private $email;

    /**
     * Email constructor.
     * @param String $email
     * @throws InvalidEmailException
     */
    public function __construct(String $email)
    {
        $this->email;
        $this->validate();
    }

    /**
     * @throws InvalidEmailException
     */
    private function validate()
    {
        $this->emptyVerify();

        $this->lengthVerify();

        $this->emailVerify();
    }

    /**
     * @throws InvalidEmailException
     */
    private function emptyVerify() : void
    {
        if (empty($this->email)) throw new InvalidEmailException('E-mail em branco');
    }

    /**
     * @throws InvalidEmailException
     */
    private function lengthVerify() : void
    {
        if (strlen($this->email) <= 3) throw new InvalidEmailException('E-mail com quantidade de caracteres inválido');
    }

    /**
     * @throws InvalidEmailException
     */
    private function emailVerify()
    {
        if (! filter_var($this->email, FILTER_VALIDATE_EMAIL)) throw new InvalidEmailException('E-mail inválido');
    }

    /**
     * @return String
     */
    public function getEmail() : String
    {
        return $this->email;
    }
}