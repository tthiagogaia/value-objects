<?php

namespace App\ValueObjects;

use App\Expcetions\InvalidCnpjException;
use App\ValueObjects\Traits\Validable;

class Cnpj
{
    use Validable;

    private $cnpj;

    /**
     * Cnpj constructor.
     * @param String $cnpj
     * @throws InvalidCnpjException
     */
    public function __construct(String $cnpj)
    {
        $this->cnpj = $cnpj;
        $this->validate();
    }

    /**
     * @throws InvalidCnpjException
     */
    private function validate() : void
    {
        $this->emptyVerify();

        $this->maskRemove();

        $this->lengthVerify();

        $this->invalidDigitsVerify();

        $this->algorithmCnpjVerify();
    }

    /**
     * @throws InvalidCnpjException
     */
    private function emptyVerify() : void
    {
        if (empty($this->cnpj)) throw new InvalidCnpjException('CNPJ em branco');
    }

    private function maskRemove() : void
    {
        $this->cnpj = preg_replace('/[^0-9]/', '', $this->cnpj);
    }

    /**
     * @throws InvalidCnpjException
     */
    private function lengthVerify()
    {
        if (strlen($this->cnpj) < 14) throw new InvalidCnpjException('CNPJ com quantidade de caracteres inválido');
    }

    /**
     * @throws InvalidCnpjException
     */
    private function invalidDigitsVerify() : void
    {
        $invalids = [
            '00000000000000',
            '11111111111111',
            '22222222222222',
            '33333333333333',
            '44444444444444',
            '55555555555555',
            '66666666666666',
            '77777777777777',
            '88888888888888',
            '99999999999999'
        ];

        if (in_array($this->cnpj, $invalids)) throw new InvalidCnpjException('CNPJ com todos os dígitos iguais');
    }

    /**
     * @throws InvalidCnpjException
     */
    public function algorithmCnpjVerify()
    {
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
            $soma += $this->cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        if ($this->cnpj{12} != ($resto < 2 ? 0 : 11 - $resto)) throw new InvalidCnpjException('CNPJ inválido');

        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
            $soma += $this->cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        if ($this->cnpj{13} != ($resto < 2 ? 0 : 11 - $resto)) throw new InvalidCnpjException('CNPJ inválido');
    }

    /**
     * @return String
     */
    public function getCnpj() : String
    {
        return $this->cnpj;
    }
}
