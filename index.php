<?php

require __DIR__ . '/vendor/autoload.php';

use App\ValueObjects\Cnpj;
use App\ValueObjects\Cpf;
use App\Expcetions\InvalidCpfException;
use App\Expcetions\InvalidCnpjException;

try {
    $cpf = new Cpf('283.188.340-70');
    echo $cpf->getCpf();
} catch (InvalidCpfException $exception) {
    echo $exception->getMessage();
} catch (Exception $exception) {
    echo $exception->getMessage();
}

try {
    $cnpj = new Cnpj('29.571.589/0001-60');
    echo $cnpj->getCnpj();
} catch (InvalidCnpjException $exception) {
    echo $exception->getMessage();
} catch (Exception $exception) {
    echo $exception->getMessage();
}
